package client

import (
	"fmt"
	"io"
	"net"
	"time"

	. "gitlab.com/illemonati/minecraft-proxy/utils"
)

type Client struct {
	ServerHost string
	ServerPort UnsignedShort
	ServerConn net.Conn
}

func NewClient(host string, port UnsignedShort) *Client {
	client := new(Client)
	client.ServerHost = host
	client.ServerPort = port
	return client
}

func (client *Client) GetServerAddrStr() string {
	return fmt.Sprintf("%s:%d", client.ServerHost, client.ServerPort)
}

func (client *Client) Connect() (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", client.GetServerAddrStr())
	client.ServerConn = conn
	go client.ListenToServerMessages()
	return
}

func (client *Client) RequestStatus() {}

func (client *Client) ListenToServerMessages() {
	readBuf := make([]byte, MaxPacketSize)
	for {
		messageLen, err := client.ServerConn.Read(readBuf)
		if err != nil {
			if err == io.EOF {
				return
			}
			fmt.Println(err)
		}

		messageBuf := PacketBytes(readBuf[0:messageLen])

		fmt.Printf("[s %d] %v\n", messageLen, messageBuf)
		messagePacket := messageBuf.Packet()
		fmt.Printf("length: %d, id %d \n", messagePacket.Length, messagePacket.PacketID)
		fmt.Println(PacketDataDecode(messagePacket.PacketID, messagePacket.Data))

		time.Sleep(time.Millisecond * 1)
	}
}
