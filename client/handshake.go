package client

import (
	. "gitlab.com/illemonati/minecraft-proxy/utils"
)

func (c *Client) HandShakeRequestStatus() {
	handshakeData := HandShakeData{
		ProtocolVersion: DefaultProtocolVersion,
		ServerAddress:   StringNFromString(c.ServerHost),
		ServerPort:      c.ServerPort,
		NextState:       HandShakeNextStateStatus,
	}

	handshakePacket := handshakeData.ToPacket()

	c.ServerConn.Write(handshakePacket.PacketBytes())

}

func (c *Client) HandShakeStatusRequest() {
	packet := NewUncompressedPacket(0x00, []byte{})

	c.ServerConn.Write(packet.PacketBytes())
}
