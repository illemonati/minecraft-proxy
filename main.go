package main

import (
	"time"

	"gitlab.com/illemonati/minecraft-proxy/client"
)

func main() {

	// testVarInt()

	proxyClient := client.NewClient("localhost", 25565)
	proxyClient.Connect()

	time.Sleep(time.Second)

	proxyClient.HandShakeRequestStatus()
	proxyClient.HandShakeStatusRequest()

	for {
		time.Sleep(time.Second * 5)
	}
}
