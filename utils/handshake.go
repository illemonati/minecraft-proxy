package utils

import (
	"bytes"
	"encoding/binary"
)

type HandShakeNextState int32

var (
	HandShakeNextStateStatus HandShakeNextState = 1
	HandShakeNextStateLogin  HandShakeNextState = 2
)

type HandShakeData struct {
	ProtocolVersion int32
	ServerAddress   StringN // n = 255
	ServerPort      UnsignedShort
	NextState       HandShakeNextState
}

func (d *HandShakeData) ToPacket() *UncompressedPacket {

	dataBuf := new(bytes.Buffer)
	dataBuf.Write(MakeVarInt(DefaultProtocolVersion))
	binary.Write(dataBuf, binary.BigEndian, d.ServerAddress.StringNBytes())
	binary.Write(dataBuf, binary.BigEndian, d.ServerPort)
	dataBuf.Write(MakeVarInt(int32(HandShakeNextStateStatus)))

	return NewUncompressedPacket(0x00, dataBuf.Bytes())
}

type HandShakeResponse struct {
	JSONResponse string
}

func DecodeHandshakeResponseBytes(buf []byte) HandShakeResponse {

	// fmt.Println(buf)
	// fmt.Println(string(buf))
	// strBuf := new(bytes.Buffer)
	// binary.Read(strBuf, binary.BigEndian, buf)
	return HandShakeResponse{
		JSONResponse: string(buf),
	}
}
