package utils

import (
	"bytes"
	"encoding/binary"
)

type PacketBytes []byte

type UncompressedPacket struct {
	Length   int32
	PacketID PacketID
	Data     []byte
}

func NewUncompressedPacket(packetID int32, data []byte) *UncompressedPacket {
	p := new(UncompressedPacket)
	p.Data = data
	p.PacketID = PacketID(packetID)
	p.Length = int32(len(MakeVarInt(packetID)) + len(p.Data))
	return p
}

func (p *UncompressedPacket) PacketBytes() PacketBytes {
	buf := new(bytes.Buffer)
	buf.Write(MakeVarInt(p.Length))
	buf.Write(MakeVarInt(int32(p.PacketID)))
	binary.Write(buf, binary.BigEndian, p.Data)

	return buf.Bytes()
}

func (buf PacketBytes) Packet() UncompressedPacket {

	packetLength, packetLengthSize := VarInt(buf).Int32()

	bufWithoutLength := buf[packetLengthSize:packetLength]

	packetID, packetIDSize := VarInt(bufWithoutLength).Int32()

	data := bufWithoutLength[packetIDSize:]

	return UncompressedPacket{
		Length:   packetLength,
		PacketID: PacketID(packetID),
		Data:     data,
	}

}
