package utils

type PacketID int32

var (
	HandShake PacketID = 0
)

func PacketDataDecode(packetID PacketID, data []byte) any {
	if packetID == HandShake {
		return DecodeHandshakeResponseBytes(data)
	}
	return nil
}
