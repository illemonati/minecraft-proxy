package utils

import (
	"bytes"
	"encoding/binary"
)

type StringN struct {
	Length     VarInt
	StringData []byte
}

type StringNBytes []byte

func StringNFromString(str string) StringN {
	return MakeStringN(int32(len(str)), str)
}

func MakeStringN(n int32, str string) StringN {
	strData := make([]byte, n)
	copy(strData, str)

	return StringN{
		Length:     MakeVarInt(n),
		StringData: strData,
	}
}

func (s *StringN) StringNBytes() StringNBytes {
	buf := new(bytes.Buffer)

	buf.Write(s.Length)
	binary.Write(buf, binary.BigEndian, s.StringData)

	return buf.Bytes()
}
