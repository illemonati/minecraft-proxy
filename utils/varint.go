package utils

import (
	"bytes"
	"fmt"
)

const segmentBits = 0x7F
const continueBit = 0x80

type VarInt []byte

func MakeVarInt(v int32) VarInt {

	buf := new(bytes.Buffer)
	for {
		if (v & ^segmentBits) == 0 {
			buf.WriteByte(byte(v))
			break
		}
		buf.WriteByte(byte((v & segmentBits) | continueBit))
		v = int32(uint32(v) >> 7)
	}

	return buf.Bytes()
}

func (v VarInt) Int32() (int32, int) {

	var result int32 = 0
	var position int32 = 0
	var currentByte byte

	vPos := 0

	for {
		currentByte = v[vPos]
		vPos++

		result |= (int32(currentByte) & segmentBits) << position

		if (currentByte & continueBit) == 0 {
			break
		}

		position += 7

		if position >= 32 {
			fmt.Println("VarInt Too big")
			break
		}
	}

	return result, vPos

}

func testVarInt() {
	a := 1232234234
	b := -122234234
	avar := MakeVarInt(int32(a))
	bvar := MakeVarInt(int32(b))

	ad, _ := avar.Int32()
	bd, _ := bvar.Int32()

	fmt.Printf("%d %d | %v %v | %d %d\n", a, b, avar, bvar, ad, bd)
}
